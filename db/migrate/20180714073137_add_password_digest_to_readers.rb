class AddPasswordDigestToReaders < ActiveRecord::Migration[5.2]
  def change
    add_column :readers, :password_digest, :string
  end
end
