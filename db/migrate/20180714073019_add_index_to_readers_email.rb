class AddIndexToReadersEmail < ActiveRecord::Migration[5.2]
  def change
  	 add_index :readers, :email, unique: true
  end
end
